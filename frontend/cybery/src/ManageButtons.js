import React from 'react';
import axios from 'axios';

export class ManageButtonsBooks extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id:this.props.id,
            name:this.props.name,
            link:this.props.link,
            yearID:this.props.yearID,
            courseID:this.props.courseID
        }
        
        this.deleteBook=()=>{
            
            var place=this;
            axios.delete('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearID+'/courses/'+place.state.courseID+
            "/books/"+place.state.id).then(function(response){
            if(response===200)
                console.log('OK'+place.state.yearID+" "+place.state.courseID)
               window.location.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
            
            
            
        }
        
    }
    
    render(){
        return(
            <button type="button " class="btn btnDelete"  onClick={this.deleteBook}>DELETE</button>
            )
    }
}

export class ManageButtonsCode extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id:this.props.id,
            name:this.props.name,
            link:this.props.link,
            yearID:this.props.yearID,
            courseID:this.props.courseID
        }
        
        this.deleteCode=()=>{
            
            var place=this;
            axios.delete('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearID+'/courses/'+place.state.courseID+
            "/codesamples/"+place.state.id).then(function(response){
            if(response===200)
                console.log('OK'+place.state.yearID+" "+place.state.courseID)
               window.location.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
            
        }
        
    }
    
    render(){
        return(
          
            <button type="button" class="btn btnDelete"  onClick={this.deleteCode}>DELETE</button>
            )
    }
}