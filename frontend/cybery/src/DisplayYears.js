import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import './DisplayYears.css';
import {Year} from './Year'





export class DisplayYears extends React.Component {
    constructor(props){
        super(props);
        this.state={
            source:[],
        }
        
      
    }
    
    componentDidMount(){
     
    }

    render(){
        let items=this.props.source.map((item, index) => 
        <div class="container-fluid">
        <div class="row ">
            <div class="col"  className="year" id={index} onClick={this.onClick}>{item.name}</div>
             <Year id={item.id} name={item.name}/>
        </div> 
        </div>
            );
         
        return(
    
        
                 <React.Fragment>
            <div>
                <div className="buttons-container">
                    {items}
                    
                </div>
                
            </div>
            </React.Fragment>
            )
    }
}