import React from 'react';
import {FacebookShare} from './FacebookShare'
import {ManageButtonsBooks} from './ManageButtons'
import {ModifyBook} from './AddBook'
export class Books extends React.Component{
    constructor(props){
        super(props);
        this.state={
            yearId:this.props.yearId,
            courseId: this.props.courseId,
            source: this.props.source
        }
    }
    
    render(){
        console.log("Book:"+this.state.source);
        var place=this;
        let books=this.state.source.map((item,index) => 
            <div>
                <a href={item.link}>{item.title}</a>
                <FacebookShare url={item.link}/>
                <ManageButtonsBooks id={item.id} name={item.title} yearID={place.state.yearId} courseID={place.state.courseId}/>
                <ModifyBook id={item.id} title={item.title} yearId={place.state.yearId} courseId={place.state.courseId}/>
            </div>);
        return(
            <React.Fragment>
                <div>
                    {books}
                </div>
            </React.Fragment>
            
            )
    }
}