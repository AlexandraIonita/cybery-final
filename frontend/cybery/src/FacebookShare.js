import { FacebookShareButton } from "react-simple-share";
import React from 'react';

export class FacebookShare extends React.Component{ 
    constructor(props){
        super(props);
        this.state={
            url:this.props.url
        }
    }
    render(){return(
        <FacebookShareButton
    url={this.state.url}
     />)
    }
  
}