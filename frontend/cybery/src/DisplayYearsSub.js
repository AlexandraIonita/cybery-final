import React from 'react';
import './DisplayYears.css';
import {Year} from './Year'
import axios from 'axios'


export class DisplayYears extends React.Component {
    constructor(props){
        super(props);
        this.state={
            years:[],
            subjects:[],
            bools:[]
        }
    }
    
    componentDidMount(){
        
       for(let i=0;i<this.state.years.length;i++)
            {
               this.state.bools[i]=false;
            }
    }

    getSubjects = function(id) {
         var place=this;
        axios.get('https://cybery-alexandraionita.c9users.io:8081/years/'+id+'/courses')
        .then(function(response){
             if(response.status === 200)
                place.state.subjects=response.data
                
            }).catch(function (error) {
                console.log(error);
            })
          
        }
    
    render(){
        var place=this;
        this.props.source.map(function(item) {     
        console.log("hei")
        place.state.years.push({ 
        "id":item.id,
        "name":item.name
        })
        });
        
         for(let i=0;i<this.state.years.length;i++)
            {
               this.getSubjects(this.state.years[i].id);
            }
        
        
        let buttons=this.state.years.map((item,index) =>
        <div>
            <button className="year" id={index}>{item.name}</button>
            <Year source={this.state.subjects}/>
        </div>);
        
        return(
            <React.Fragment>
            <div>
                <div className="buttons-container">
                    {buttons}
                    
                </div>
                
            </div>
            </React.Fragment>
            )
    }
}