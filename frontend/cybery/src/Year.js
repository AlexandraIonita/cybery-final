import React, { Component } from 'react';
import axios from 'axios';
import {MaterialButtons} from './MaterialsButtons'
import {ModifySubjects} from './ManageSubjects'

export class Year extends Component{
    constructor(props){
        super(props);
        this.state={
            id:this.props.id,
            subjects:[]
            }
            
        this.getSubjects = () => {
        var place=this;
        axios.get('https://cybery-alexandraionita.c9users.io:8081/years/'+this.state.id+'/courses')
        .then(function(response){
             if(response.status === 200)
                place.setState({
                    subjects : response.data
                 })
            }).catch(function (error) {
                console.log(error);
            })
          
        }
    }
    
   
    
    componentDidMount(){
       
       this.getSubjects();
    }
    
   
    
    
    render(){
       
         let subjects=this.state.subjects.map((item,index) => 
         <div>
        <div class="txtcls">{item.name}</div>
        <ModifySubjects id={item.id} name={item.name} noSeminars={item.noSeminars} noCourses={item.noCourses} semester={item.semester} yearId={item.yearId}/>
        <MaterialButtons id={item.id} name={item.name} yearID={item.yearId}/>
        </div>);
        return (
            <React.Fragment>
            
                <div>
                {subjects}
                </div>
            
            </React.Fragment>
            
     )   
    }
}
