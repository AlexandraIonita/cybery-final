import React from 'react';
import {FacebookShare} from './FacebookShare'
import {ManageButtonsCode} from './ManageButtons'
export class Code extends React.Component{
    constructor(props){
        super(props);
        this.state={
            yearId:this.props.yearId,
            courseId: this.props.courseId,
            source: this.props.source
        }
    }
    
    render(){
        var place=this;
        let codes=this.state.source.map((item,index) => 
            <div>
                <a href={item.link}>{item.name}</a>
                <FacebookShare url={item.link}/>
                <ManageButtonsCode id={item.id} name={item.name} yearID={place.state.yearId} courseID={place.state.courseId}/>
            </div>);
        return(
            <React.Fragment>
                <p>Code Samples</p>
                <div>
                    {codes}
                </div>
            </React.Fragment>
            
            )
    }
}