import React from 'react';
import "./App.css"
import {DisplayYears} from './DisplayYears'
import axios from 'axios';
import {AddBook} from './AddBook'
import {AddCode} from './AddBook'
import {AddSubjects} from './ManageSubjects'

class App extends React.Component {
  

constructor(props){
  super(props);
  this.state={};
  this.state.years=[];
  this.state.subjects=[];
  
}


//get years from db
getYearsFromDB = () => {
  var place=this;
  axios.get("https://cybery-alexandraionita.c9users.io:8081/years",{
  }).then(function(response){
    if(response.status === 200)
      place.setState({years: response.data})
  }).catch(function (error){
    console.log(error);
  })
  
};




 



componentDidMount(){
  this.getYearsFromDB();

}





render(){
  return(
    <div>
    <h1 id="myh">Welcome to Cybery</h1>
    <h2 id="myh">Your online guide to surviving and thriving</h2>
     <DisplayYears source={this.state.years}/>
    <div>
    <AddSubjects/>
    <AddBook />
    <AddCode />
    </div>
    </div>
  
    );
}
}


export default App;