import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import axios from 'axios';

export class AddBook extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isShowing:false,
            title:'',
            link:'',
            courseId:-1,
            yearId:-1
        }
        
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
    this.addToDB=()=>{
        var place=this;
        axios.post('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearId+'/courses/'+place.state.courseId+
            "/books/",{
                "title":place.state.title,
                "link":place.state.link
            }).then(function(response){
            if(response===200)
                console.log('OK'+place.state.yearID+" "+place.state.courseID)
               window.location.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
            this.toggleAdd();
            
    }
    }
     toggleAdd=()=>{
        this.setState({
            isShowing:!this.state.isShowing
        })
        
        
        
        console.log(this.state.isShowing);
    }
    render(){
        
        return(
            <React.Fragment>
        <button type="button" class="btn" id="btnBook" onClick={this.toggleAdd}>Add Book</button>
        {this.state.isShowing && <div>
        <form>
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="as">Title</span>
                </div>
                <input type="text" class="form-control" id="title" aria-describedby="as" name="title" onChange={this.handleChange} />
            </div>
            
             <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="bs">Link</span>
                </div>
                <input type="text" class="form-control" id="link" name="link"aria-describedby="bs" onChange={this.handleChange} />
            </div>
            
             <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="cs">Year</span>
                </div>
                <input type="number" id="yearId" class="form-control" name="yearId" aria-describedby="cs" onChange={this.handleChange} />
            </div>
            
             <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="ds">Course</span>
                </div>
                <input type="number" class="form-control" aria-describedby="ds" id="courseId" name="courseId" onChange={this.handleChange} />
             </div>
             
          <button type="button"  class="btn btnAddpestetot" onClick={this.addToDB}>Add</button>
        </form>
      </div>
        }
         </React.Fragment>
            )
    }
    
    
    
}


export class AddCode extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isShowing:false,
            name:'',
            link:'',
            courseId:-1,
            yearId:-1
        }
        
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
    this.addToDB=()=>{
        var place=this;
        axios.post('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearId+'/courses/'+place.state.courseId+
            "/codesamples/",{
                "name":place.state.name,
                "link":place.state.link
            }).then(function(response){
            if(response===200)
                console.log('OK')
               window.location.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
            
            this.toggleAdd();
    }
    }
     toggleAdd=()=>{
        this.setState({
            isShowing:!this.state.isShowing
        })
        
        console.log(this.state.isShowing);
    }
    render(){
        
        return(
            <React.Fragment>
        <button type="button" class="btn" onClick={this.toggleAdd}>Add Code</button>
        {this.state.isShowing && <div>
        <form>
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="ax">Title</span>
                </div>
                <input type="text" class="form-control" aria-describedby="ax" id="name" name="name" onChange={this.handleChange} />
            </div>
          
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="bx">Link</span>
                </div>
                <input type="text" id="link" class="form-control" aria-describedby="bx" name="link" onChange={this.handleChange} />
            </div>
            
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="cx">Year</span>
                </div>
                <input type="number" id="yearId" class="form-control" aria-describedby="cx" name="yearId" onChange={this.handleChange} />
            </div>
          
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="dx">Course</span>
                </div>
                <input type="number" class="form-control" aria-describedby="dx" id="courseId" name="courseId" onChange={this.handleChange} />
            </div>
          
          <button type="button"  class="btn btnAddpestetot"  onClick={this.addToDB}>Add</button>
        </form>
      </div>
        }
         </React.Fragment>
            )
    }
}

export class ModifyBook extends React.Component{
     constructor(props){
        super(props);
        this.state={
            isShowing:false,
            id:this.props.id,
            title:this.props.title,
            link:this.props.link,
            courseId:this.props.courseId,
            yearId:this.props.yearId
        }
        
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
    this.addToDB=()=>{
        var place=this;
        axios.put('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearId+'/courses/'+place.state.courseId+
            "/books/"+place.state.id,{
                "title":place.state.title,
                "link":place.state.link
            }).then(function(response){
            if(response===200)
                console.log('OK'+place.state.yearID+" "+place.state.courseID)
               window.location.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
            this.toggleAdd();
    }
    }
     toggleAdd=()=>{
        this.setState({
            isShowing:!this.state.isShowing
        })
        
        console.log(this.state.isShowing);
    }
    render(){
        
        return(
            <React.Fragment>
        <button type="button"  class="btn" id="btnModify" onClick={this.toggleAdd}>Modify</button>
        {this.state.isShowing && <div>
        <form>
          <label for="title">Title</label>
          <input type="text" id="title" name="title" onChange={this.handleChange} />
          <label for="link">Link</label>
          <input type="text" id="link" name="link" onChange={this.handleChange} />
          <button type="button" class="btn btnModify"  onClick={this.addToDB}>Modify</button>
        </form>
      </div>
        }
         </React.Fragment>
            )
    }
    
}