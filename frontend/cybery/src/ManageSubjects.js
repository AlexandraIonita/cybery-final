import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import axios from 'axios';
import './ManageSubjects.css';


export class AddSubjects extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isShowing:false,
            name:'',
            noSeminars:'',
            noCourses:-1,
            semester:1,
            yearId:-1
        }
        
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
    this.addToDB=()=>{
        var place=this;
        axios.post('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearId+'/courses/'
            ,{
                "name":place.state.name,
                "noSeminars":place.state.noSeminars,
                "noCourses":place.state.noCourses,
                "semester":place.state.semester,
            }).then(function(response){
            if(response===200)
                console.log('OK')
               window.location.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
            this.toggleAdd();
    }
    }
     toggleAdd=()=>{
        this.setState({
            isShowing:!this.state.isShowing
        })
        
        console.log(this.state.isShowing);
    }
    render(){
        
        return(
            <React.Fragment>
        <button type="button" class="btn" onClick={this.toggleAdd}>Add Subject</button>
        {this.state.isShowing && <div>
        <form>
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend ">
                    <span class="input-group-text " id="bb">Name</span>
                </div>
                <input type="text" class="form-control" id="name" name="name"  aria-describedby="bb" onChange={this.handleChange} />
            </div>
            
            <div class="input-group mb-3 w-25">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="aa" >Seminars</span>
                </div>
                <input type="number" class="form-control" id="noSeminars" name="noSeminars" aria-describedby="aa" onChange={this.handleChange} />
            </div>
          
           <div class="input-group mb-3 w-25">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="cc">Courses</span>
                </div>
                <input type="number" class="form-control" id="noCourses" name="noCourses" aria-describedby="cc" onChange={this.handleChange} />
                </div>
          
           <div class="input-group mb-3 w-25">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="dd">Year</span>
                </div>
                <input type="number" class="form-control" id="yearId" name="yearId" aria-describedby="dd" onChange={this.handleChange} />
          </div>
          
            
            <button type="button" class="btn btnAddpestetot"  onClick={this.addToDB}>Add</button>
            
        </form>
     </div>
        }
         </React.Fragment>
            )
    }
    
    
    
}

export class ModifySubjects extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isShowing:false,
            id:this.props.id,
            name:this.props.name,
            noSeminars:this.props.noSeminars,
            noCourses:this.props.noCourses,
            semester:this.props.semester,
            yearId:this.props.yearId
        }
        
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name] : evt.target.value
      })
    }
    this.addToDB=()=>{
        var place=this;
        axios.put('https://cybery-alexandraionita.c9users.io:8081/years/'+place.state.yearId+'/courses/0'+place.state.id
            ,{
                "name":place.state.name,
                "noSeminars":place.state.noSeminars,
                "noCourses":place.state.noCourses,
                "semester":place.state.semester,
            }).then(function(response){
            if(response===200)
                console.log('OK')
               window.localStorage.reload();
            }).catch(function (error) {
                console.log(error);
                  console.log('OK'+place.state.yearID+" "+place.state.courseID+" "+place.state.id);
            })
        this.toggleAdd();
    }
    }
     toggleAdd=()=>{
        this.setState({
            isShowing:!this.state.isShowing
        })
        
        console.log(this.state.isShowing);
    }
    render(){
        
        return(
            <React.Fragment>
        <button type="button" class="btn btnModifySubject"  onClick={this.toggleAdd}>Modify </button>
        {this.state.isShowing && <div>
        <form>
            <div class="input-group mb-3 ">
                <div class="input-group-prepend">           
                    <span class="input-group-text" id="a">Name</span>
                </div>
               <input type="text" class="form-control" id="name" aria-describedby="a" name="name" onChange={this.handleChange} />
            </div>
          
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="b">Seminars</span>
                </div>
                <input type="number" id="noSeminars" class="form-control" name="noSeminars" aria-describedby="b" onChange={this.handleChange} />
            </div>
          
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="c">Courses</span>
                </div>
            <input type="number" class="form-control" id="noCourses" name="noCourses" aria-describedby="c" onChange={this.handleChange} />
          </div>
          
          <button type="button" class="btn" onClick={this.addToDB} id="btnDone">Done</button>
        </form>
      </div>
        }
         </React.Fragment>
            )
    }
    
    
    
}