import React from 'react';
import axios from 'axios';
import {Books} from './Book'
import {Code} from './Code'
export class MaterialButtons extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id:this.props.id,
            name:this.props.name,
            yearID:this.props.yearID,
            books:[],
            code:[],
            displayMaterials:false,
            displayCode: false
        }
                
        
        this.fctgetBooks=()=>{
        
        var place=this;
        
        axios.get('https://cybery-alexandraionita.c9users.io:8081/years/'+this.state.yearID+'/courses/'+this.state.id+'/books')
        .then(function(response){
            if(response.status === 200){
                place.setState({
                    books:response.data
                })
            }
        }).catch(function (error) {
                console.log(error);
            })
        
    }
    
     this.fctgetCode=()=>{
        
        var place=this;
        
        axios.get('https://cybery-alexandraionita.c9users.io:8081/years/'+this.state.yearID+'/courses/'+this.state.id+'/codesamples')
        .then(function(response){
            if(response.status === 200){
                place.setState({
                    code:response.data
                })
            }
        }).catch(function (error) {
                console.log(error);
            })
        
    }
    }
    
    toggleHidden = () =>{
        this.setState({
            displayMaterials: !this.state.displayMaterials,
        })
        
    }
    
     toggleHiddenCode = () =>{
        this.setState({
            displayCode: !this.state.displayCode,
        })
        
    }
   
    
    componentDidMount(){
        this.fctgetBooks();
        this.fctgetCode();
    }
    render(){
        
        return(
            <React.Fragment>
            <div>
            <button type="button" class="btn getCB" id={"getMaterials"+this.state.id}  onClick={this.toggleHidden}>Get books</button>
            <button type="button" class="btn getCB" id={"getMaterials"+this.state.id} onClick={this.toggleHiddenCode}>Get code </button>
            </div>
            {
                this.state.displayMaterials && <div><Books yearId={this.state.yearID} courseId={this.state.id} source={this.state.books}/></div>
            }
            

            {
                this.state.displayCode && <div><Code yearId={this.state.yearID} courseId={this.state.id} source={this.state.code}/></div>
            }
           
              </React.Fragment>
            )
    }
}