'use strict'
const express=require('express')
const bodyParser=require('body-parser')
const Sequelize = require('sequelize')




const sequelize = new Sequelize('ProjectDataBase','root','',{
	dialect : 'mysql',
	operatorsAliases: false,
	define : {
		timestamps : false
	}
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
 
 const app=express();
app.use(bodyParser.json());

app.use(function(req, res, next){ 
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next(); 
}); 

const YearofStudy = sequelize.define('year',{
    id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
    name: {type: Sequelize.STRING, allowNull: false},
    noSubjects: Sequelize.INTEGER
});

const Course = sequelize.define('course',{
     id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
    name: {type: Sequelize.STRING, allowNull: false},
    noSeminars: Sequelize.INTEGER,
    noCourses: Sequelize.INTEGER,
    semester: Sequelize.STRING
    
});

YearofStudy.hasMany(Course);

const Book = sequelize.define('book', {
     id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
	title : {type: Sequelize.STRING, allowNull: false},
	link : Sequelize.STRING,
    });
    
Course.hasMany(Book);

const CodeSample=sequelize.define('codeSample',{
     id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
    name: {type: Sequelize.STRING, allowNull: false},
    link:Sequelize.STRING
});

Course.hasMany(CodeSample);


app.get("/", (req,res)=>{
   res.status(200).send("Welcome to Cybery!"); 
});

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true});
		res.status(201).json({message : 'created'});
	}
	catch(e){
		console.warn(e);
		res.status(500).json({message : 'server error'});
	}
});

app.post('/putYears', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await YearofStudy.bulkCreate(req.body);
			res.status(201).json({message : 'created'});
		}
		else{
			await YearofStudy.create(req.body);
			res.status(201).json({message : 'created'});
		}
	}
	catch(e){
		console.warn(e);
		res.status(500).json({message : 'server error'});
	}
})

app.get('/years', async (req, res) => {
	try{
		let years = await YearofStudy.findAll()
		res.status(200).json(years)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:id', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.id)
		if (year){
			res.status(200).json(year)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/years/:id', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.id)
		if (year){
			await year.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/years/:id', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.id)
		if (year){
			await year.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:yid/courses', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if (year){
			let courses = await year.getCourses()
			res.status(200).json(courses)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:yid/courses/:cid', async (req, res) => {
	try{
		let year= await YearofStudy.findById(req.params.yid)
		if (year){
			let courses = await year.getCourses({where : {id : req.params.cid}})
			if(courses.shift())
				res.status(200).json(courses.shift())
			else
				res.status(404).json({message : 'course not found'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/years/:yid/courses', async (req, res) => {
	try{
		let year= await YearofStudy.findById(req.params.yid)
		if (year){
			let course = req.body
			course.yearId = year.id
			await Course.create(course)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/years/:yid/courses/:cid', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if (year){
			let courses = await year.getCourses({where : {id : req.params.cid}})
			let course = courses.shift()
			if (course){
				await course.update(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/years/:yid/courses/:cid', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if (year){
			let courses = await year.getCourses({where : {id : req.params.cid}})
			let course = courses.shift()
			if (course){
				await course.destroy(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/years/:yid/courses/:cid/books', async (req, res) => {
	try{
		let year =await YearofStudy.findById(req.params.yid)
		if(year){
			let courses= await year.getCourses({where: {id : req.params.cid}})
			let course = courses.shift()
			if(course){
				console.log(course.name)
				let book=req.body
				book.courseId=course.id
				await Book.create(book)
				res.status(201).json({message : 'created'})
			}
			else{
				res.status(404).json({message : 'course not found'})
			}
		}
		else
		{
			res.status(404).json({message : 'year not found'})
		}
		
		
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:yid/courses/:cid/books', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				console.log(course.name)
				let books = await course.getBooks()
				res.status(200).json(books)
			}
			else
			{
				res.status(404).json({message : 'course not found'})
			}
		}
		else{
			res.status(404).json({message : 'year not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:yid/courses/:cid/books/:bid', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				
				let books = await course.getBooks({where: {id : req.params.bid}})
				res.status(200).json(books.shift())
			}
			else
			{
				res.status(404).json({message : 'course not found'})
			}
		}
		else{
			res.status(404).json({message : 'year not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/years/:yid/courses/:cid/books/:bid', async (req, res) => {
	try{
	let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				let books = await course.getBooks({where: {id : req.params.bid}})
				let book=books.shift()
				if(book){
					await book.update(req.body)
					res.status(202).json({message : 'accepted'})
				}
					else{
				res.status(404).json({message : 'not found'})
			}
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
				res.status(404).json({message : 'not found'})
			}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/years/:yid/courses/:cid/books/:bid', async(req, res) => {
	try{
			let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				let books = await course.getBooks({where: {id : req.params.bid}})
				let book=books.shift()
				if(book){
					await book.destroy(req.body)
					res.status(202).json({message : 'accepted'})
				}
				else{
				res.status(404).json({message : 'book not found'})
			}
			}
			else{
				res.status(404).json({message : 'course not found'})
			}
			}
			
			else{
				res.status(404).json({message : 'year not found'})
			}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})



app.post('/years/:yid/courses/:cid/codesamples', async (req, res) => {
	try{
		let year =await YearofStudy.findById(req.params.yid)
		if(year){
			let courses= await year.getCourses({where: {id : req.params.cid}})
			let course = courses.shift()
			if(course){
				let code=req.body
				code.courseId=course.id
				await CodeSample.create(code)
				res.status(201).json({message : 'created'})
			}
			else{
				res.status(404).json({message : 'course not found'})
			}
		}
		else
		{
			res.status(404).json({message : 'year not found'})
		}
		
		
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:yid/courses/:cid/codesamples', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				let codes = await course.getCodeSamples()
				res.status(200).json(codes)
			}
			else
			{
				res.status(404).json({message : 'course not found'})
			}
		}
		else{
			res.status(404).json({message : 'year not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/years/:yid/courses/:cid/codesamples/:csid', async (req, res) => {
	try{
		let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				
				let codes = await course.getCodeSamples({where: {id : req.params.csid}})
				res.status(200).json(codes.shift())
			}
			else
			{
				res.status(404).json({message : 'course not found'})
			}
		}
		else{
			res.status(404).json({message : 'year not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/years/:yid/courses/:cid/codesamples/:csid', async (req, res) => {
	try{
	let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				let codes = await course.getCodeSamples({where: {id : req.params.csid}})
				let code=codes.shift()
				if(code){
					await code.update(req.body)
					res.status(202).json({message : 'accepted'})
				}
					else{
				res.status(404).json({message : 'not found'})
			}
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
				res.status(404).json({message : 'not found'})
			}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/years/:yid/courses/:cid/codesamples/:csid', async(req, res) => {
	try{
			let year = await YearofStudy.findById(req.params.yid)
		if(year){
			let courses=await year.getCourses({where: {id : req.params.cid}});
			let course = courses.shift()
			if(course){
				let codes = await course.getCodeSamples({where: {id : req.params.csid}})
				let code=codes.shift()
				if(code){
					await code.destroy(req.body)
					res.status(202).json({message : 'accepted'})
				}
				else{
				res.status(404).json({message : 'book not found'})
			}
			}
			else{
				res.status(404).json({message : 'course not found'})
			}
			}
			
			else{
				res.status(404).json({message : 'year not found'})
			}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})


app.listen(8081, ()=>{
    console.log('Server started on port 8081...');
})